/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DDTRPROCESSOR_H
#define SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DDTRPROCESSOR_H

#include "cheetah/ddtr/gpu_bruteforce/detail/DedispersionStrategy.cuh"
#include "cheetah/ddtr/gpu_bruteforce/DedispersionPlan.h"
#include "cheetah/data/cuda/FrequencyTime.h"
#include "cheetah/data/DmTrials.h"
#include <vector>
#ifdef SKA_CHEETAH_ENABLE_CUDA

namespace ska {
namespace cheetah {
namespace ddtr {
namespace gpu_bruteforce {

/**
 * @brief Ddtr processor object with ++ operator which essentially calls the DDTR kernel
 * @details This object takes in FT object and fills up dmtrials object
 */

template<typename DdtrTraits>
class DdtrProcessor
{
        typedef typename DdtrTraits::value_type NumericalRep;
        typedef typename DdtrTraits::DmTrialsType DmTrialsType;
        typedef DedispersionStrategy<Cpu, NumericalRep> DedispersionStrategyType;
        typedef DedispersionStrategy<cheetah::Cuda, typename DedispersionStrategyType::NumericalRep> GpuStrategyType;

    public:
        /**
         * @brief Contructor for DdtrProcessor
         * @param dt DDTR strategy object
         * @param ft FT object which points to the data on GPU
         * @param dmt DMtrails object resultant DM-Time data to be stored on host
         * @param copy_flag: true if data to be moved to host this flag can be set in config
         * @param current_dm_range the index of the DM range that must be processed.
         */
        DdtrProcessor( DedispersionStrategyType const& dt
                     , data::FrequencyTime<cheetah::Cuda, NumericalRep>& ft
                     , std::shared_ptr<DmTrialsType> dmt
                     , bool copy_flag
                     , size_t& current_dm_range
                     );

        /**
         * @brief process the next step of the DedispersionPlan
         */
        DdtrProcessor& operator++();

        /**
         * @return true if there is no more processing to do
         */
        bool finished() const;

        /**
         * @return Gpu DedispersionStrategy object
         *
         */
        GpuStrategyType& gpu_strategy();

        /**
         * @return DedispersionStrategy object
         *
         */
        DedispersionStrategyType const& dedispersion_strategy() const;

        /**
         * @return current_dm_range
         *
         */
        std::size_t const& current_dm_range() const;

    private:
        GpuStrategyType _gpu_strategy;
        data::FrequencyTime<cheetah::Cuda, NumericalRep>& _device_ft_data;
        DedispersionStrategyType const&  _strategy;
        std::shared_ptr<DmTrialsType> _dm_trials_ptr;
        bool _copy_dm_trials;
        std::size_t& _current_dm_range;
};


} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace cheetah
} // namespace ska
#include "detail/DdtrProcessor.cpp"

#endif //SKA_CHEETAH_ENABLE_CUDA
#endif // SKA_CHEETAH_DDTR_GPU_BRUTEFORCE_DDTRPROCESSOR_H
