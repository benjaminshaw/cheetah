#ifndef SKA_CHEETAH_DRED_CUDA_DETAIL_NORMALISER_H
#define SKA_CHEETAH_DRED_CUDA_DETAIL_NORMALISER_H

#include "cheetah/cuda_utils/cuda_thrust.h"

namespace ska {
namespace cheetah {
namespace dred {
namespace cuda {
namespace detail {

/**
 * @brief      Functor for normalising spectral data
 *
 * @tparam     T     Base value type of complex data to be normalised
 */
template <typename T>
struct PowerNormalisingFunctor
    : public thrust::binary_function<thrust::complex<T>, T, thrust::complex<T>>
{
    inline __host__ __device__
    thrust::complex<T> operator()(thrust::complex<T> input, T local_median) const;
};

} // namespace detail
} // namespace cuda
} // namespace dred
} // namespace cheetah
} // namespace ska

#include "cheetah/dred/cuda/detail/Normaliser.cu"

#endif // SKA_CHEETAH_DETAIL_NORMALISER_H