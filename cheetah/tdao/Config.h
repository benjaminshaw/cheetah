/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_TDAO_CONFIG_H
#define SKA_CHEETAH_TDAO_CONFIG_H

#include "cheetah/tdao/cuda/Config.h"
#include "cheetah/utils/Config.h"
#include "cheetah/data/Units.h"

namespace ska {
namespace cheetah {
namespace tdao {

/**
 * @brief
 *
 * @details
 *
 */
class Config : public cheetah::utils::Config
{
    public:
        Config();
        ~Config();

        /**
         * @brief      The cuda implementation configuration for Tdao
         */
        cuda::Config const& cuda_config() const;

        /**
         * @brief      The significance threshold for candidates
         */
        float significance_threshold() const;
        void significance_threshold(float);

        /**
         * @brief      The minumum frequency to search for candidates at
         */
        data::FourierFrequencyType minimum_frequency() const;
        void minimum_frequency(data::FourierFrequencyType const&);

        /**
         * @brief      The maximum frequency to search for candidates at
         */
        data::FourierFrequencyType maximum_frequency() const;
        void maximum_frequency(data::FourierFrequencyType const&);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        cuda::Config _cuda_config;
        float _threshold;
        data::FourierFrequencyType _min_frequency;
        data::FourierFrequencyType _max_frequency;
};

} // namespace tdao
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_TDAO_CONFIG_H
