include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_tdas_src
    src/AccListGenConfigTest.cpp
    src/gtest_tdas.cpp
)

add_executable(gtest_tdas ${gtest_tdas_src})
target_link_libraries(gtest_tdas ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_tdas gtest_tdas --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
