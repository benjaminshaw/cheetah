subpackage(cuda)
subpackage(altera)

set(MODULE_FFT_LIB_SRC_CUDA
    ${LIB_SRC_CUDA}
    PARENT_SCOPE
)

set(MODULE_FFT_LIB_SRC_CPU
    src/Config.cpp
    src/CommonPlan.cpp
    src/Plan.cpp
    ${LIB_SRC_CPU}
    PARENT_SCOPE
)

test_utils()
add_subdirectory(test)
